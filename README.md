This project was created as a research internship in the Antwerp University, the purpose of this project was to translate an algorithm for significant subgraph mining made in Perl to a Java language. Bellow you can see a brief description of this repository taken from https://bitbucket.org/pmeysman/sigsubgraphminer :

"The Significant Subgraph Miner (SSM) is an algorithm to find subgraphs in a single graph that are significantly associated with a set of vertices. It can handle a large variety of graph dataset types and vertex selections. Further it efficiently progresses through the search space so that most problems should be computable.

The accompanying implementation is provided for free for research purposes.
Some bugs may be present within the software and no guarantees are given!
We would appreciate any comments, bug descriptions, suggestions or succes stories regarding the tool.

This implementation should work on most regular size biological networks with any number of selected vertices and several node labels as demonstrated in the accompanying publication. The only requirement to run this version is a standard Perl 5+ installation and it has been extensively tested on Linux and Mac OSX systems. The example dataset is included in the zip file for illustrative and testing purposes."

In order to run the code is necessary to have java installed and the following is the command which can run the algoritm by having the jar file:

 java -jar subgraphmining.jar [+ the options bellow]

The available options for the algorithm are: 

 -h, ”help”, ”Print help”

 -g, ”graph”, ”path of the graph”

 -l, ”labels”, ”path of the labels” 

 -p, ”groupfile”, ”path of the group file”

 -b, ”bgFile”, ”path of the bgfile”

 -s, ”support”, ”expected support of the algorithm” 

 -i, ”singleLabel”, ”Variant where each node has exactly one label and this label must exactly match for the motif” 

 -u, ”undirected”, ”Undirected option where A-¿B = B-¿A and self-loops aren’t allowed” 

 -d, ”debug”, ”debug” 

 -m, ”maxsize”, ”Maximum number of vertixes allowed in the

subgraph”
 -v, ”pvalue”, ”Maximum pvalue allowed (default 0.05”

 -n, ”nestedpvalue”, ”Variant where the significance of the child motif is based on the parent matches” 

 -o, ”output”, ”Outputfile to print significant motifs”

The algorithm was translated by
Gerardo Orellana
gerar.ore@gmail.com