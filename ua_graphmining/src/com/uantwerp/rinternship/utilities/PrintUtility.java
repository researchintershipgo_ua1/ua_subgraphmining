package com.uantwerp.rinternship.utilities;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import com.uantwerp.rinternship.main.Graph;

public class PrintUtility {

	public static void printHasMap(HashMap<String,String> hashToPtint){
		System.out.println("printing new hash");
		Iterator<String> it = hashToPtint.keySet().iterator();
		while (it.hasNext()){
			String key = (String) it.next();
			String value = hashToPtint.get(key);
			System.out.println(key+" "+value);
		}
	}
	
	public static void printHashSet(HashSet<String> hashToPtint){
		System.out.println("printing new hash");
		Iterator<String> it = hashToPtint.iterator();
		while (it.hasNext()){
			String key = (String) it.next();
			System.out.println(key);
		}
	}
	
	public static void printHasMap2(HashMap<String,Integer> hashToPtint){
		System.out.println("printing new hash");
		Iterator<String> it = hashToPtint.keySet().iterator();
		while (it.hasNext()){
			String key = (String) it.next();
			int value = hashToPtint.get(key);
			System.out.println(key+" "+value);
		}
	}
	
	public static void printHasMapHashSet(HashMap<String,HashSet<String>> hashToPtint){
		System.out.println("printing new hash with hasSet");
		Iterator<String> it = hashToPtint.keySet().iterator();
		while (it.hasNext()){
			String key = (String) it.next();
			HashSet<String> value = hashToPtint.get(key);
			Iterator<String> itint = value.iterator();
			while (itint.hasNext()){
				String valueIntSet = (String) itint.next();
				System.out.println(key+" "+valueIntSet);
			}
		}
	}
	
	public static void printListString(List<String> list){
		for (int i=0;i < list.size(); i++){
			System.out.println(list.get(i));
		}
	}
	
	public static void printHSetString(HashSet<String> hSet){
		Iterator<String> it = hSet.iterator();
		while (it.hasNext()){
			String key = (String) it.next();
			System.out.println(key);
		}
	}
	
//	private static void realSizeLabel(){
//		int size = 0;
//		String nodes = "";
//		Iterator<String> it = Graph.label.keySet().iterator();
//		while (it.hasNext()){
//			String key = it.next();
//			HashSet<String> internalNodes = Graph.label.get(key);
//			size+=internalNodes.size();
//			Iterator<String> it2 = internalNodes.iterator();
//			while (it2.hasNext()){
//				nodes += key + ";" + it2.next() + "\n";
//			}
//		}
//		System.out.println("Found: " + size + " nodes in graph");
//		String location = "/Users/gerardo/Google Drive/antwerp_university/thesis/code/datasets/SSM_nodes_java.csv";
//		FileUtility.writeFile(location, nodes);
//	}
	
	public static void printSummary(){
		System.out.println("Found: " + Graph.label.size() + " nodes in graph");
		System.out.println("Of which " + Graph.bgnodes.size()+" are part of the background");
		System.out.println("Of which " + Graph.group.size()+" were selected");
		System.out.println("Of which " + Graph.graphHash.size()+" have targets");
		System.out.println("Of which " + Graph.reverseGraphHash.size()+" are targets");
		System.out.println("With " + Graph.labelHash.size() + " possible labels");
	}
}
