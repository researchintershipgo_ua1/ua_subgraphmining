package com.uantwerp.rinternship.utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.uantwerp.rinternship.main.Graph;
import com.uantwerp.rinternship.main.OptimizeParameter;

public class MapUtil
{
    
    public static  List<Map.Entry<String, OptimizeParameter>> sortByValue( HashMap<String, OptimizeParameter> map)
    {
    	List<Map.Entry<String, OptimizeParameter>> list = new ArrayList<Map.Entry<String, OptimizeParameter>>( map.entrySet() );
        Collections.sort( list, new Comparator<Map.Entry<String, OptimizeParameter>>()
        {
            public int compare( Map.Entry<String, OptimizeParameter> o1, Map.Entry<String, OptimizeParameter> o2)
            {
            	com.uantwerp.rinternship.main.OptimizeParameter op1 = (com.uantwerp.rinternship.main.OptimizeParameter) o1.getValue();
            	com.uantwerp.rinternship.main.OptimizeParameter op2 = (com.uantwerp.rinternship.main.OptimizeParameter) o2.getValue();

            	if (op1.getLenghtToOne() == op2.getLenghtToOne())
            		if (op1.getOutgoingEdges() == op2.getOutgoingEdges())
            			if (op1.getIncomingEdges() == op2.getIncomingEdges()){
            				if (Graph.labelHash.get(op1.getLabel())==Graph.labelHash.get(op2.getLabel()))
            					return o1.getKey().compareTo(o2.getKey());
            				else if (Graph.labelHash.get(op1.getLabel()) > Graph.labelHash.get(op2.getLabel()))
            					return 1;
            				else
            					return -1;
            			}
            			else
            				if (op1.getIncomingEdges() > op2.getIncomingEdges())
                    			return -1;
                    		else
                    			return 1;                			
            		else
            			if (op1.getOutgoingEdges() > op2.getOutgoingEdges())
                			return -1;
                		else 
                			return 1;
            	else
            		if (op1.getLenghtToOne() > op2.getLenghtToOne())
            			return 1;
            		else
            			return -1;
            }
        } );
        return list;
    }
    
    public static  List<Map.Entry<String, OptimizeParameter>> sortByValue_und( HashMap<String, OptimizeParameter> map)
    {
    	List<Map.Entry<String, OptimizeParameter>> list = new ArrayList<Map.Entry<String, OptimizeParameter>>( map.entrySet() );
        Collections.sort( list, new Comparator<Map.Entry<String, OptimizeParameter>>()
        {
            public int compare( Map.Entry<String, OptimizeParameter> o1, Map.Entry<String, OptimizeParameter> o2)
            {
            	com.uantwerp.rinternship.main.OptimizeParameter op1 = (com.uantwerp.rinternship.main.OptimizeParameter) o1.getValue();
            	com.uantwerp.rinternship.main.OptimizeParameter op2 = (com.uantwerp.rinternship.main.OptimizeParameter) o2.getValue();

            	if (op1.getLenghtToOne()==op2.getLenghtToOne())
            		if (op1.getOutgoingEdges()==op2.getOutgoingEdges()){
        				if (Graph.labelHash.get(op1.getLabel()) == Graph.labelHash.get(op2.getLabel()))
        					return o1.getKey().compareTo(o2.getKey());
        				else if (Graph.labelHash.get(op1.getLabel()) > Graph.labelHash.get(op2.getLabel()))
        					return 1;
        				else
        					return -1;
        			}               			
            		else
            			if (op1.getOutgoingEdges() > op2.getOutgoingEdges())
                			return -1;
                		else 
                			return 1;
            	else
            		if (op1.getLenghtToOne() > op2.getLenghtToOne())
            			return 1;
            		else
            			return -1;
            }
        } );
        return list;
    }
    
    public static  List<String> sortByValueHashSet( HashSet<String> map, final HashMap<String,String> order)
    {
    	List<String> list = new ArrayList<String>(map);
        Collections.sort(list, new Comparator<String>() 
        {
            public int compare(String o1, String o2)
            {
            	return order.get(o1).compareTo(order.get(o2));
            }
        } );
        return list;
    }
    

    
}