package com.uantwerp.rinternship.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.uantwerp.rinternship.exceptions.SubGraphMiningException;
import com.uantwerp.rinternship.main.EdgePair;
import com.uantwerp.rinternship.main.EdgesLoop;
import com.uantwerp.rinternship.main.Graph;
import com.uantwerp.rinternship.main.GraphPathParameters;
import com.uantwerp.rinternship.main.OptimizeParameter;

public abstract class AlgorithmUtility {
	
	public static void checkGraphFile(String graph){
		if (graph.equals(""))
			SubGraphMiningException.exceptionEmptyFile();		
	}
	
	public static boolean checkEmptyGraph(String graph){
		if (graph == null)
			return false;
		if (graph.equals(""))
			return false;	
		return true;
	}
	
	public static List<String> getPossibleLables(int singleLabel,HashMap<String, HashSet<String>> reverselabels){
		List<String> possibleLabels = new ArrayList<String>();
		if (singleLabel==0){
			possibleLabels.add("~");
		}
		Set<String> keys = reverselabels.keySet();
	    Iterator<String> itr = keys.iterator();
	    while (itr.hasNext()) { 
	        possibleLabels.add(itr.next());
	     } 
		return possibleLabels;
	}
	
	//Support threshold calculation
	public static int supportTreshold(){
		int supportcutoffResult=0;
		if (GraphPathParameters.supportcutoff == 0){
			//Estimate number of subgraphs to be tested
			Double d = Math.pow(GraphPathParameters.maxsize,2)/2 ;
			double stimate = (Math.pow(     2,(     d.intValue()   )    ) *  Math.pow(Graph.possibleLabels.size(),GraphPathParameters.maxsize));
			double corrpval = GraphPathParameters.pvalue/stimate;
			//Estimate corresponding support
			for (int i = 1; i <= Graph.group.size();i++){
				double prob = HypergeomDist.getProbability(Graph.bgnodes.size() - Graph.group.size(), Graph.group.size(), i, i);
				if (prob < corrpval){
					supportcutoffResult = i;
					break;
				}
			}
			if (supportcutoffResult>0){
				System.out.println("Subgraph support set at " + supportcutoffResult + " due to upper bound Pvalue");
			}
			else{
				supportcutoffResult = Graph.group.size();//interestingVertices;
				System.out.println("Subgraph support set at " + supportcutoffResult + " for number of interesting vertices\n");
			}
		}else{
			return GraphPathParameters.supportcutoff;
		}
		return supportcutoffResult;
	}
	
	public static HashMap<String,HashSet<String>> addCheckElementHash(HashMap<String,HashSet<String>> graphHash, String key, String element){
		if (key != null && element != null){
			if (graphHash.containsKey(key)){
				HashSet<String> elementMod = graphHash.get(key);
				elementMod.add(element);
				graphHash.put(key,elementMod);
			}
			else{
				HashSet<String> newElement = new HashSet<String>();
				newElement.add(element);
				graphHash.put(key,newElement);
			}
		}
		return graphHash;
	}
	
	public static HashSet<String> returnKeySetHash(HashMap<String,HashSet<String>> hash){
		HashSet<String> hSet = new HashSet<String>();
		Iterator<String> it = hash.keySet().iterator();
		while (it.hasNext()){
			String key = (String) it.next();
			hSet.add(key);
		}
		return hSet;
	}
	
	public static List<String> getValuesFromHash(HashMap<String,String> hash){
		List<String> list = new ArrayList<>();
		Iterator<String> it = hash.keySet().iterator();
		while(it.hasNext()){
			String keyValue = it.next();
			if (!list.contains(hash.get(keyValue)))
				list.add(hash.get(keyValue));
		}
		return list;
	}
	
	public static String getStringMotifs(List<EdgePair> motifs){
		String totalConstruc ="";
		for (int i=0; i<motifs.size(); i++){
			if (totalConstruc.equals("")){
				totalConstruc = motifs.get(i).getSourceId()+motifs.get(i).getSourceLabel()+"-"+motifs.get(i).getTargetId()+motifs.get(i).getTargetLabel();
			}else{
				totalConstruc = totalConstruc+","+motifs.get(i).getSourceId()+motifs.get(i).getSourceLabel()+"-"+motifs.get(i).getTargetId()+motifs.get(i).getTargetLabel();
			}
		}
		return totalConstruc;
	}
	
	public static boolean checkContainsEdge(List<EdgePair> edges, EdgePair edge){
		boolean check = false;
		for (int i=0; i < edges.size(); i++){
			if(edges.get(i).getSourceId().equals(edge.getSourceId())
					&& edges.get(i).getSourceLabel().equals(edge.getSourceLabel())
					&& edges.get(i).getTargetId().equals(edge.getTargetId())
					&& edges.get(i).getTargetLabel().equals(edge.getTargetLabel()))
				return true;
		}
		return check;
	}
	
	public static double getProbability(HashSet<String> prevhits, int checkedgroupnodes, int totalsupport, int hitSupport){
		double prob = 0.0;
		if (GraphPathParameters.nestedpval==1){
			//Nested P-value behaviour: Enrichment P-value is calculated against parent subgraph matches
			prob = HypergeomDist.getProbability(prevhits.size()-checkedgroupnodes,checkedgroupnodes, totalsupport, hitSupport);

		}else{
			//Normal behaviour: Enrichment p-value is calculated against the entire graph.
			prob = HypergeomDist.getProbability(Graph.bgnodes.size()-Graph.group.size(),Graph.group.size(), totalsupport, hitSupport);
		}
		return prob;
	}
	
	public static EdgesLoop generateEdges(List<EdgePair> motifref){
		HashSet<String> seen = new HashSet<>();
		EdgesLoop edges = new EdgesLoop();
		/*for created to check if there is a loop in the graph*/
		for (int i=0; i < motifref.size(); i++){
			EdgePair ep = motifref.get(i);
			if ((seen.contains(ep.getSourceId())) && (seen.contains(ep.getTargetId()))){
				if (ep.getSourceId().compareTo(ep.getTargetId())>0){
					edges.setFowloopedge(AlgorithmUtility.addCheckElementHash(edges.getFowloopedge(), ep.getSourceId(), ep.getTargetId()));
				}else{
					edges.setBackloopedge(AlgorithmUtility.addCheckElementHash(edges.getBackloopedge(), ep.getTargetId(), ep.getSourceId()));
				}
			}
			seen.add(ep.getSourceId());
			seen.add(ep.getTargetId());
			edges.getEdgecountbynode().put(ep.getSourceId(), edges.getEdgecountbynode().get(ep.getSourceId())==null ? 1 : edges.getEdgecountbynode().get(ep.getSourceId())+1);
			if (GraphPathParameters.undirected==0)
				edges.getEdgecountbytarget().put(ep.getTargetId(), edges.getEdgecountbytarget().get(ep.getTargetId())==null ? 1 : edges.getEdgecountbytarget().get(ep.getTargetId())+1);
			else
				edges.getEdgecountbynode().put(ep.getTargetId(), edges.getEdgecountbynode().get(ep.getTargetId())==null ? 1 : edges.getEdgecountbynode().get(ep.getTargetId())+1);
		}
		return edges;
	}
	
	public static boolean checkTargetLabel(String graphtarget, String targetLabel){
		if (GraphPathParameters.singleLabel == 1){
			if (!Graph.label.containsKey(graphtarget)){
				return true;
			}
		}
		HashSet<String> labelsTargetGraph = Graph.label.get(graphtarget);
		if (!labelsTargetGraph.contains(targetLabel)){
			return true;
		}
		return false;
	}
	
	public static boolean checkLoops(String nodeId, HashMap<String, HashSet<String>> fowloopedge, HashMap<String, HashSet<String>> backloopedge, HashMap<String,String> match, String graphtarget){
		if (fowloopedge.containsKey(nodeId)){
			Iterator<String> it = fowloopedge.get(nodeId).iterator();
			while (it.hasNext()){
				String fowlooptarget = it.next();
				if (match.containsKey(fowlooptarget)){
					if (GraphPathParameters.undirected == 1){
						boolean cond1 = false;
						boolean cond2 = false;
						if (Graph.graphHash.containsKey(graphtarget))
							if (match.containsKey(fowlooptarget))
								cond1 = !Graph.graphHash.get(graphtarget).contains(match.get(fowlooptarget));
						if (Graph.reverseGraphHash.containsKey(graphtarget))
							if (match.containsKey(fowlooptarget))
								cond2 = !Graph.reverseGraphHash.get(graphtarget).contains(match.get(fowlooptarget));
						if (cond1 && cond2)
							return true;
					}else{
						if (!Graph.graphHash.containsKey(graphtarget))
							return true;
						if (!Graph.graphHash.get(graphtarget).contains(match.get(fowlooptarget)))
							return true;
					}
				}
			}
		}
		if (backloopedge.containsKey(nodeId)){
			Iterator<String> it = backloopedge.get(nodeId).iterator();
			while (it.hasNext()){
				String backlooptarget = it.next();
				if (match.containsKey(backlooptarget)){
					if (GraphPathParameters.undirected == 1){
						boolean cond1 = false;
						boolean cond2 = false;
						if (Graph.graphHash.containsKey(graphtarget))
							if (match.containsKey(backlooptarget))
								cond1 = !Graph.graphHash.get(graphtarget).contains(match.get(backlooptarget));
						if (Graph.reverseGraphHash.containsKey(graphtarget))
							if (match.containsKey(backlooptarget))
								cond2 = !Graph.reverseGraphHash.get(graphtarget).contains(match.get(backlooptarget));
						if (cond1 && cond2)
							return true;
					}else{
						if (!Graph.reverseGraphHash.containsKey(graphtarget))
							return true;
						if (!Graph.reverseGraphHash.get(graphtarget).contains(match.get(backlooptarget)))
							return true;
					}
				}
			}
		}
		return false;
	}
	
	public static void updateParams(String key, String label, double value, int type, Map<String,OptimizeParameter> params){//0 lenghttoone, 1 incomingEdges, 2 outgoingEdges
		OptimizeParameter op =new OptimizeParameter(label);
		if (params.containsKey(key))
			op = params.get(key);
		else
			op =new OptimizeParameter(label);
		if (type==0){
			op.setLenghtToOne(value);
		}else if (type == 1){
			op.setIncomingEdges(op.getIncomingEdges()+1);
		}else{
			op.setOutgoingEdges(op.getOutgoingEdges()+1);
		}
		params.put(key, op);
	}
}
