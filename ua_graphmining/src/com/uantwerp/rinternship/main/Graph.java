package com.uantwerp.rinternship.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public abstract class Graph {

	public static HashMap<String, HashSet<String>> graphHash = new HashMap<String, HashSet<String>>();
	public static HashMap<String, HashSet<String>> reverseGraphHash = new HashMap<String, HashSet<String>>();
	public static HashSet<String> group = new HashSet<String>();
	public static HashSet<String> bgnodes = new HashSet<String>();
	public static HashMap<String, Integer> labelHash = new HashMap<String, Integer>();
	public static HashMap<String, HashSet<String>> reverseLabel = new HashMap<String, HashSet<String>>();
	public static HashMap<String, HashSet<String>> label = new HashMap<String, HashSet<String>>();
	public static List<String> possibleLabels = new ArrayList<>();
	
	public static void resetGraph(){
		graphHash = new HashMap<String, HashSet<String>>();
		reverseGraphHash = new HashMap<String, HashSet<String>>();
		group = new HashSet<String>();
		bgnodes = new HashSet<String>();
		labelHash = new HashMap<String, Integer>();
		reverseLabel = new HashMap<String, HashSet<String>>();
		label = new HashMap<String, HashSet<String>>();
		possibleLabels = new ArrayList<>();
	}
}