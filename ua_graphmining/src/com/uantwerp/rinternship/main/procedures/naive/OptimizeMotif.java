/**
 * 
 */
package com.uantwerp.rinternship.main.procedures.naive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.uantwerp.rinternship.exceptions.SubGraphMiningException;
import com.uantwerp.rinternship.main.EdgePair;
import com.uantwerp.rinternship.main.GraphPathParameters;
import com.uantwerp.rinternship.main.OptimizeParameter;
import com.uantwerp.rinternship.utilities.AlgorithmUtility;
import com.uantwerp.rinternship.utilities.MapUtil;

/**
 * @author gerardo
 *
 */
public abstract class OptimizeMotif {

	public static List<EdgePair> optimizeMotif(List<EdgePair> motifs){
		HashMap<String,OptimizeParameter> params = new HashMap<String, OptimizeParameter>(4);
		HashMap<String,HashSet<String>> edgeByNode = new HashMap<>();
		HashMap<String,HashSet<String>> edgeByTarget = new HashMap<>();
		HashMap<String, String> motifLabel = new HashMap<>();
		for (int i=0; i < motifs.size(); i++){
			motifLabel.put(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel());
			motifLabel.put(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel());
			edgeByNode=AlgorithmUtility.addCheckElementHash(edgeByNode, motifs.get(i).getSourceId(), motifs.get(i).getTargetId());
			edgeByTarget=AlgorithmUtility.addCheckElementHash(edgeByTarget, motifs.get(i).getTargetId(),motifs.get(i).getSourceId());
			if (motifs.get(i).getSourceId().equals("1")){
				if (!motifs.get(i).getTargetId().equals("1")){
					AlgorithmUtility.updateParams(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel(), 1, 0, params);
					AlgorithmUtility.updateParams(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel(), 0, 1, params);
				}
			}else if (motifs.get(i).getTargetId().equals("1")){
				AlgorithmUtility.updateParams(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel(), 0.9, 0, params);
				AlgorithmUtility.updateParams(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel(), 0, 2, params);
			}else{
				AlgorithmUtility.updateParams(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel(), 0, 1, params);
				AlgorithmUtility.updateParams(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel(), 0, 2, params);
				
				if ((params.get(motifs.get(i).getSourceId()).getLenghtToOne()!=0) 
						&& (params.get(motifs.get(i).getTargetId()).getLenghtToOne()!=0)){
					if  (params.get(motifs.get(i).getTargetId()).getLenghtToOne() > params.get(motifs.get(i).getSourceId()).getLenghtToOne()){
						AlgorithmUtility.updateParams(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel(), params.get(motifs.get(i).getSourceId()).getLenghtToOne()+1, 0, params);
					}
					if  (params.get(motifs.get(i).getSourceId()).getLenghtToOne() > params.get(motifs.get(i).getTargetId()).getLenghtToOne()){
						AlgorithmUtility.updateParams(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel(), params.get(motifs.get(i).getTargetId()).getLenghtToOne()+1, 0, params);
					}
				}else if (params.get(motifs.get(i).getSourceId()).getLenghtToOne()!=0){
					AlgorithmUtility.updateParams(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel(), params.get(motifs.get(i).getSourceId()).getLenghtToOne()+1, 0, params);
				}else if (params.get(motifs.get(i).getTargetId()).getLenghtToOne()!=0){
					AlgorithmUtility.updateParams(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel(), params.get(motifs.get(i).getTargetId()).getLenghtToOne()+1, 0, params);
				}else{
					//This should not happen as motifs are constructed away from the root node; but just in case
					AlgorithmUtility.updateParams(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel(), GraphPathParameters.maxsize, 0, params);
					AlgorithmUtility.updateParams(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel(), GraphPathParameters.maxsize, 0, params);
				}
			}
		}

		//Sort source nodes based on the distance to the root node, number of outgoing edges and the label
		HashMap<String,String> transform = new HashMap<String, String>();
		transform.put("1", "1");
		List<Map.Entry<String, OptimizeParameter>> neworder = MapUtil.sortByValue(params);
		for (int i = 0; i < neworder.size(); i++){
			if (neworder.get(i).getKey().equals("1"))
				SubGraphMiningException.exceptionOldId(String.valueOf(i+2));
			transform.put(neworder.get(i).getKey(), String.valueOf(i+2));
		}
		List<EdgePair> newMotif = new ArrayList<>();
		if (edgeByNode.containsKey("1")){
			if (edgeByNode.get("1").contains("1")){
				newMotif.add(new EdgePair("1", motifLabel.get("1"), "1", motifLabel.get("1")));
			}
		}
		
		//Sorted by proximity to the root node and number of edges
		ListIterator<Map.Entry<String, OptimizeParameter>> listIterator = neworder.listIterator();
		while (listIterator.hasNext()){
			Map.Entry<String, OptimizeParameter> entry = listIterator.next();
			//only save edge if node is source node, or root node is source node
			if (edgeByNode.containsKey("1")){
				if (edgeByNode.get("1").contains(entry.getKey())){
					newMotif.add(new EdgePair("1", motifLabel.get("1"), transform.get(entry.getKey()), motifLabel.get(entry.getKey())));
				}
			}
			if (edgeByNode.containsKey(entry.getKey()))
				if (edgeByNode.get(entry.getKey()).contains("1")){
					newMotif.add(new EdgePair(transform.get(entry.getKey()), motifLabel.get(entry.getKey()),"1", motifLabel.get("1")));
				}
			if (!edgeByNode.containsKey(entry.getKey()) && !edgeByTarget.containsKey(entry.getKey()))
				continue;
			if (edgeByNode.containsKey(entry.getKey())){
				List<String> orderEdgeByNode = MapUtil.sortByValueHashSet(edgeByNode.get(entry.getKey()), transform);
				for (int i = 0; i < orderEdgeByNode.size(); i++)
					if (transform.get(entry.getKey()).compareTo(transform.get(orderEdgeByNode.get(i))) <= 0){
						newMotif.add(new EdgePair(transform.get(entry.getKey()), motifLabel.get(entry.getKey()),transform.get(orderEdgeByNode.get(i)),motifLabel.get(orderEdgeByNode.get(i))));
					}
			}
			if (edgeByTarget.containsKey(entry.getKey())){
				List<String> orderEdgeByTarget = MapUtil.sortByValueHashSet(edgeByTarget.get(entry.getKey()), transform);
				for (int i = 0; i < orderEdgeByTarget.size(); i++)
					if (transform.get(entry.getKey()).compareTo(transform.get(orderEdgeByTarget.get(i))) < 0){
						newMotif.add(new EdgePair(transform.get(orderEdgeByTarget.get(i)),motifLabel.get(orderEdgeByTarget.get(i)),transform.get(entry.getKey()), motifLabel.get(entry.getKey())));
					}
			}
		}
		return newMotif;
	}
	
	public static List<EdgePair> optimizeMotif_und(List<EdgePair> motifs){
		HashMap<String,OptimizeParameter> params = new HashMap<String, OptimizeParameter>(4);
		HashMap<String,HashSet<String>> edgeByNode = new HashMap<>();
		HashMap<String,HashSet<String>> edgeByTarget = new HashMap<>();
		HashMap<String, String> motifLabel = new HashMap<>();
		for (int i=0; i < motifs.size(); i++){
			motifLabel.put(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel());
			motifLabel.put(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel());
			edgeByNode=AlgorithmUtility.addCheckElementHash(edgeByNode, motifs.get(i).getSourceId(), motifs.get(i).getTargetId());
			edgeByTarget=AlgorithmUtility.addCheckElementHash(edgeByTarget, motifs.get(i).getTargetId(),motifs.get(i).getSourceId());
			if (motifs.get(i).getSourceId().equals("1")){
				if (!motifs.get(i).getTargetId().equals("1")){
					AlgorithmUtility.updateParams(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel(), 1, 0, params);
					AlgorithmUtility.updateParams(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel(), 0, 1, params);
				}
			}else if (motifs.get(i).getTargetId().equals("1")){
				AlgorithmUtility.updateParams(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel(), 1, 0, params);
				AlgorithmUtility.updateParams(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel(), 0, 2, params);
			}else{
				AlgorithmUtility.updateParams(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel(), 0, 2, params);
				AlgorithmUtility.updateParams(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel(), 0, 2, params);
				
				if ((params.get(motifs.get(i).getSourceId()).getLenghtToOne()!=0) 
						&& (params.get(motifs.get(i).getTargetId()).getLenghtToOne()!=0)){
					if  (params.get(motifs.get(i).getTargetId()).getLenghtToOne() > params.get(motifs.get(i).getSourceId()).getLenghtToOne()){
						AlgorithmUtility.updateParams(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel(), params.get(motifs.get(i).getSourceId()).getLenghtToOne()+1, 0, params);
					}
					if  (params.get(motifs.get(i).getSourceId()).getLenghtToOne() > params.get(motifs.get(i).getTargetId()).getLenghtToOne()){
						AlgorithmUtility.updateParams(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel(), params.get(motifs.get(i).getTargetId()).getLenghtToOne()+1, 0, params);
					}
				}else if (params.get(motifs.get(i).getSourceId()).getLenghtToOne()!=0){
					AlgorithmUtility.updateParams(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel(), params.get(motifs.get(i).getSourceId()).getLenghtToOne()+1, 0, params);
				}else if (params.get(motifs.get(i).getTargetId()).getLenghtToOne()!=0){
					AlgorithmUtility.updateParams(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel(), params.get(motifs.get(i).getTargetId()).getLenghtToOne()+1, 0, params);
				}else{
					//This should not happen as motifs are constructed away from the root node; but just in case
					AlgorithmUtility.updateParams(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel(), GraphPathParameters.maxsize, 0, params);
					AlgorithmUtility.updateParams(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel(), GraphPathParameters.maxsize, 0, params);
				}
			}
		}

		//Sort source nodes based on the distance to the root node, number of outgoing edges and the label
		HashMap<String,String> transform = new HashMap<String, String>();
		transform.put("1", "1");
		List<Map.Entry<String, OptimizeParameter>> neworder = MapUtil.sortByValue_und(params);
		for (int i = 0; i < neworder.size(); i++){
			if (neworder.get(i).getKey().equals("1"))
				SubGraphMiningException.exceptionOldId(String.valueOf(i+2));
			transform.put(neworder.get(i).getKey(), String.valueOf(i+2));
		}
		List<EdgePair> newMotif = new ArrayList<>();
		if (edgeByNode.containsKey("1")){
			if (edgeByNode.get("1").contains("1")){
				newMotif.add(new EdgePair("1", motifLabel.get("1"), "1", motifLabel.get("1")));
			}
		}
		
		//Sorted by proximity to the root node and number of edges
		ListIterator<Map.Entry<String, OptimizeParameter>> listIterator = neworder.listIterator();
		while (listIterator.hasNext()){
			Map.Entry<String, OptimizeParameter> entry = listIterator.next();
			//only save edge if node is source node, or root node is source node
			if (edgeByNode.containsKey("1")){
				if (edgeByNode.get("1").contains(entry.getKey()))
					newMotif.add(new EdgePair("1", motifLabel.get("1"), transform.get(entry.getKey()), motifLabel.get(entry.getKey())));
			}
			if (edgeByNode.containsKey(entry.getKey()))
				if (edgeByNode.get(entry.getKey()).contains("1"))
					newMotif.add(new EdgePair("1", motifLabel.get("1"),transform.get(entry.getKey()), motifLabel.get(entry.getKey())));
			if (!edgeByNode.containsKey(entry.getKey()) && !edgeByTarget.containsKey(entry.getKey()))
				continue;
			if (edgeByNode.containsKey(entry.getKey())){
				List<String> orderEdgeByNode = MapUtil.sortByValueHashSet(edgeByNode.get(entry.getKey()), transform);
				for (int i = 0; i < orderEdgeByNode.size(); i++)
					if (transform.get(entry.getKey()).compareTo(transform.get(orderEdgeByNode.get(i))) <= 0)
						newMotif.add(new EdgePair(transform.get(entry.getKey()), motifLabel.get(entry.getKey()),transform.get(orderEdgeByNode.get(i)),motifLabel.get(orderEdgeByNode.get(i))));
			}
			if (edgeByTarget.containsKey(entry.getKey())){
				List<String> orderEdgeByTarget = MapUtil.sortByValueHashSet(edgeByTarget.get(entry.getKey()), transform);
				for (int i = 0; i < orderEdgeByTarget.size(); i++)
					if (transform.get(entry.getKey()).compareTo(transform.get(orderEdgeByTarget.get(i))) < 0)
						newMotif.add(new EdgePair(transform.get(entry.getKey()), motifLabel.get(entry.getKey()),transform.get(orderEdgeByTarget.get(i)),motifLabel.get(orderEdgeByTarget.get(i))));
			}
		}
		return newMotif;
	}
}
