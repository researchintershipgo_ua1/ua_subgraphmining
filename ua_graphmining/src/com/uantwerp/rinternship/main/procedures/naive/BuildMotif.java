/**
 * 
 */
package com.uantwerp.rinternship.main.procedures.naive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import com.uantwerp.rinternship.main.EdgePair;
import com.uantwerp.rinternship.main.Graph;
import com.uantwerp.rinternship.main.GraphPathParameters;
import com.uantwerp.rinternship.main.MiningState;
import com.uantwerp.rinternship.utilities.AlgorithmUtility;

/**
 * @author gerardo
 *
 */
public abstract class BuildMotif {

	public static int build_motif(List<EdgePair> motifs, HashSet<String> prevhits){
		HashSet<String> hitNodes = new HashSet<>();
		HashMap<String,String> motiflabels = new HashMap<>();
		HashMap<String,Integer> nodetargets = new HashMap<>();
		HashMap<String,Integer> nodesources = new HashMap<>();
		String motifString = AlgorithmUtility.getStringMotifs(motifs);

		for (int i = 0; i < motifs.size(); i++){
			motiflabels.put(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel());
			motiflabels.put(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel());
			nodetargets.put(motifs.get(i).getTargetId(), (nodetargets.containsKey(motifs.get(i).getTargetId())?nodetargets.get(motifs.get(i).getTargetId())+1:1));
			nodesources.put(motifs.get(i).getSourceId(), (nodesources.containsKey(motifs.get(i).getSourceId())?nodesources.get(motifs.get(i).getSourceId())+1:1));
		}
		int maxId = motiflabels.size();
		if (GraphPathParameters.debug==1) System.out.println("Starting on "+motifString);
		int hitSupport = 0;
		int checkedgroupnodes = 0;
		
		Iterator<String> it = Graph.group.iterator();
		checkgroup: while(it.hasNext()){//Check each interesting node if they are a root node
			String keyValue = it.next();
			//Check if part of the prevhit set
			if(!prevhits.contains(keyValue)){ continue checkgroup; }	
			
			checkedgroupnodes++;
			
			//Check if first node matches label (otherwise skip)
			if (!Graph.label.get(keyValue).contains(motiflabels.get("1"))){continue checkgroup;	}

			HashMap<String,String> supmatch = new HashMap<>();
			supmatch.put("1", keyValue);
			HashMap<String, String> supmatchref = new HashMap<>();
			supmatchref = MatchSubgraph.matchSubgraph(motifs,supmatch);
			if (supmatchref!=null){
				hitSupport++;
				hitNodes.add(keyValue);
			}
		}
		
		MiningState.checkedmotifs.put(motifString, hitSupport);
		
		int biggerfim = 0;
		//Stop with this subgraph if it did not pass the required support
		if (hitSupport < GraphPathParameters.supportcutoff){ 
			return biggerfim;
		}
		
		biggerfim++;
		int totalsupport = 0; //Support with all nodes

		Iterator<String> it2 = prevhits.iterator();
		while (it2.hasNext()){
			String node = it2.next();
			//Check if already run with group nodes
			if (Graph.group.contains(node)){
				if (hitNodes.contains(node)){
					totalsupport++;
				}
				continue;
			}
			//Check if first node matches label (otherwise skip) and has the required edges
			if (!Graph.label.get(node).contains(motiflabels.get("1"))){continue;}
			if (nodesources.containsKey("1")){
				if (Graph.graphHash.containsKey(node)){
					if (Graph.graphHash.get(node).size() < nodesources.get("1")) continue;
				}else continue;
			}
			if (nodetargets.containsKey("1")){
				if (Graph.reverseGraphHash.containsKey(node)){
					if (Graph.reverseGraphHash.get(node).size() < nodetargets.get("1")) continue;
				}else continue;
			}
			
			HashMap<String,String> supmatch = new HashMap<>();
			supmatch.put("1", node);
			HashMap<String, String> supmatchref = new HashMap<>();
			supmatchref = MatchSubgraph.matchSubgraph(motifs,supmatch);
			if (supmatchref!=null){
				totalsupport++;
				hitNodes.add(node);
			}
		}
		
		//Calculate significance
		double prob = AlgorithmUtility.getProbability(prevhits,checkedgroupnodes, totalsupport, hitSupport);
		
		if(GraphPathParameters.debug==1) System.out.println(motifString+"\t"+hitSupport+"\t"+totalsupport+"\t"+prob);
		
		MiningState.sigmotifs.put(motifString, prob);
		MiningState.freqmotifs.put(motifString, totalsupport);
		
		if (motifs.size() >= GraphPathParameters.maxsize)
			return biggerfim;
		
		Iterator<String> it3 = motiflabels.keySet().iterator();
		while (it3.hasNext()){
			List<EdgePair> potentialedges = new ArrayList<>();
			String sourceId = it3.next();
			
			//Try connecting each node in motif to each other node
			Iterator<String> it4 = motiflabels.keySet().iterator();
			LABEL2: while (it4.hasNext()){
				String targetId = it4.next();
				EdgePair edge = new EdgePair(sourceId, motiflabels.get(sourceId), targetId, motiflabels.get(targetId));
				if (AlgorithmUtility.checkContainsEdge(motifs, edge)) 
					continue LABEL2;
				else 
					potentialedges.add(edge);
			}
			
			//Try adding a new edge with every possible label
			for (int i = 0; i<Graph.possibleLabels.size(); i++){
				EdgePair forwardedge = new EdgePair(sourceId, motiflabels.get(sourceId), String.valueOf(maxId+1), Graph.possibleLabels.get(i));
				potentialedges.add(forwardedge);
				EdgePair backwardedge = new EdgePair(String.valueOf(maxId+1), Graph.possibleLabels.get(i),sourceId, motiflabels.get(sourceId));
				potentialedges.add(backwardedge);
			}
			
			TARGETLOOP: for(int i=0; i<potentialedges.size(); i++){
				List<EdgePair> newmotif = new ArrayList<>();
				newmotif.addAll(motifs);
				newmotif.add(potentialedges.get(i));
				String newmotifString = AlgorithmUtility.getStringMotifs(newmotif);
				List<EdgePair> optnewmotif = new ArrayList<>();
				if(MiningState.motiftransformations.containsKey(newmotifString)){
					optnewmotif = newmotif;
				}else{
					optnewmotif = OptimizeMotif.optimizeMotif(newmotif);
					MiningState.motiftransformations.put(newmotifString, AlgorithmUtility.getStringMotifs(optnewmotif));
					if (GraphPathParameters.debug==1) System.out.println("Transformed "+AlgorithmUtility.getStringMotifs(newmotif)+" into: "+AlgorithmUtility.getStringMotifs(optnewmotif));
						newmotifString = AlgorithmUtility.getStringMotifs(optnewmotif);
					}
					if (MiningState.checkedmotifs.containsKey(newmotifString)){
						continue TARGETLOOP;
					}else{
					build_motif(optnewmotif, hitNodes);
					biggerfim++;
				}

			}
		}
		return biggerfim;
	}
	
	public static int build_motif_und(List<EdgePair> motifs, HashSet<String> prevhits){
		HashSet<String> hitNodes = new HashSet<>();
		HashMap<String,String> motiflabels = new HashMap<>();
		HashMap<String,Integer> nodesources = new HashMap<>();
		String motifString = AlgorithmUtility.getStringMotifs(motifs);
		
		for (int i = 0; i < motifs.size(); i++){
			motiflabels.put(motifs.get(i).getSourceId(), motifs.get(i).getSourceLabel());
			motiflabels.put(motifs.get(i).getTargetId(), motifs.get(i).getTargetLabel());
			nodesources.put(motifs.get(i).getTargetId(), (nodesources.containsKey(motifs.get(i).getTargetId())?nodesources.get(motifs.get(i).getTargetId())+1:1));
			nodesources.put(motifs.get(i).getSourceId(), (nodesources.containsKey(motifs.get(i).getSourceId())?nodesources.get(motifs.get(i).getSourceId())+1:1));
		}
		int maxId = motiflabels.size();
		if (GraphPathParameters.debug==1) System.out.println("Starting on "+ motifString);
		int hitSupport = 0;
		int checkedgroupnodes = 0;
		
		Iterator<String> it = Graph.group.iterator();
		checkgroup: while(it.hasNext()){//Check each interesting node if they are a root node
			String keyValue = it.next();
			//Check if part of the prevhit set
			if(!prevhits.contains(keyValue)){ continue checkgroup; }	
			
			checkedgroupnodes++;
			
			//Check if first node matches label (otherwise skip)
			if (!Graph.label.get(keyValue).contains(motiflabels.get("1"))){ continue checkgroup;	}
			
			HashMap<String,String> supmatch = new HashMap<>();
			supmatch.put("1", keyValue);
			HashMap<String, String> supmatchref = new HashMap<>();
			supmatchref = MatchSubgraph.matchSubgraph_und(motifs,supmatch);
			if(supmatchref!=null){
				hitSupport++;
				hitNodes.add(keyValue);
			}
		}
		
		MiningState.checkedmotifs.put(motifString, hitSupport);
		
		int biggerfim = 0;
		//Stop with this subgraph if it did not pass the required support
		if (hitSupport < GraphPathParameters.supportcutoff){ 
			return biggerfim;
		}
		
		biggerfim++;
		int totalsupport = 0; //Support with all nodes
		Iterator<String> it2 = prevhits.iterator();
		while (it2.hasNext()){
			String node = it2.next();
			//Check if already run with group nodes
			if (Graph.group.contains(node)){
				if (hitNodes.contains(node)){
					totalsupport++;
				}
				continue;
			}
			//Check if first node matches label (otherwise skip) and has the required edges
			if (!Graph.label.get(node).contains(motiflabels.get("1"))){ continue; }
			if (nodesources.containsKey("1")){
				int c = 0;
				if (Graph.graphHash.containsKey(node))
					c += Graph.graphHash.get(node).size();
				if (Graph.reverseGraphHash.containsKey(node))
					c += Graph.reverseGraphHash.get(node).size();
				if (c < nodesources.get("1")) { continue; }
			}
			
			HashMap<String,String> supmatch = new HashMap<>();
			supmatch.put("1", node);
			HashMap<String, String> supmatchref = new HashMap<>();
			supmatchref = MatchSubgraph.matchSubgraph_und(motifs,supmatch);
			if(supmatchref!=null){
				totalsupport++;
				hitNodes.add(node);
			}
		}
		
		//Calculate significance
		double prob = AlgorithmUtility.getProbability(prevhits,checkedgroupnodes, totalsupport, hitSupport);
		
		if(GraphPathParameters.debug==1) System.out.println(motifString+"\t"+hitSupport+"\t"+totalsupport+"\t"+prob);
		
		MiningState.sigmotifs.put(motifString, prob);
		MiningState.freqmotifs.put(motifString, totalsupport);
		
		if (motifs.size() >= GraphPathParameters.maxsize)
			return biggerfim;
		
		Iterator<String> it3 = motiflabels.keySet().iterator();
		while (it3.hasNext()){
			List<EdgePair> potentialedges = new ArrayList<>();
			String sourceId = it3.next();
			
			//Try connecting each node in motif to each other node
			Iterator<String> it4 = motiflabels.keySet().iterator();
			LABEL2: while (it4.hasNext()){
				String targetId = it4.next();

				if (sourceId.compareTo(targetId)>=0) { continue LABEL2; }
				
				EdgePair edge = new EdgePair(sourceId, motiflabels.get(sourceId), targetId, motiflabels.get(targetId));
				if (AlgorithmUtility.checkContainsEdge(motifs, edge)) 
					continue LABEL2;
				else 
					potentialedges.add(edge);
			}
			
			//Try adding a new edge with every possible label
			for (int i = 0; i<Graph.possibleLabels.size(); i++){
				EdgePair forwardedge = new EdgePair(sourceId, motiflabels.get(sourceId), String.valueOf(maxId+1), Graph.possibleLabels.get(i));
				potentialedges.add(forwardedge);
			}
			
			TARGETLOOP: for(int i=0; i<potentialedges.size(); i++){
				List<EdgePair> newmotif = new ArrayList<>();
				newmotif.addAll(motifs);
				newmotif.add(potentialedges.get(i));
				String newmotifString = AlgorithmUtility.getStringMotifs(newmotif);
				List<EdgePair> optnewmotif = new ArrayList<>();
				if(MiningState.motiftransformations.containsKey(newmotifString)){
					optnewmotif = newmotif;
				}else{
					optnewmotif = OptimizeMotif.optimizeMotif_und(newmotif);
					MiningState.motiftransformations.put(newmotifString, AlgorithmUtility.getStringMotifs(optnewmotif));
					
					if (GraphPathParameters.debug==1) System.out.println("Transformed "+AlgorithmUtility.getStringMotifs(newmotif)+" into: "+AlgorithmUtility.getStringMotifs(optnewmotif));
					newmotifString = AlgorithmUtility.getStringMotifs(optnewmotif);
				}
				if (MiningState.checkedmotifs.containsKey(newmotifString)){
					continue TARGETLOOP;
				}else{
					build_motif_und(optnewmotif, hitNodes);
					biggerfim++;
				}
			}
		}
		return biggerfim;
	}
}
