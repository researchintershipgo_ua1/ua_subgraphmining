package com.uantwerp.rinternship.main;

import java.util.HashMap;
import java.util.HashSet;

public class EdgesLoop {

	HashMap<String,Integer> edgecountbynode = new HashMap<>();
	HashMap<String,Integer> edgecountbytarget = new HashMap<>();
	HashMap<String, HashSet<String>> fowloopedge = new HashMap<>();
	HashMap<String, HashSet<String>> backloopedge = new HashMap<>();
	
	public EdgesLoop() {
		super();
	}
	
	public HashMap<String, Integer> getEdgecountbynode() {
		return edgecountbynode;
	}
	
	public void setEdgecountbynode(HashMap<String, Integer> edgecountbynode) {
		this.edgecountbynode = edgecountbynode;
	}
	
	public HashMap<String, Integer> getEdgecountbytarget() {
		return edgecountbytarget;
	}
	
	public void setEdgecountbytarget(HashMap<String, Integer> edgecountbytarget) {
		this.edgecountbytarget = edgecountbytarget;
	}

	public HashMap<String, HashSet<String>> getFowloopedge() {
		return fowloopedge;
	}

	public void setFowloopedge(HashMap<String, HashSet<String>> fowloopedge) {
		this.fowloopedge = fowloopedge;
	}

	public HashMap<String, HashSet<String>> getBackloopedge() {
		return backloopedge;
	}

	public void setBackloopedge(HashMap<String, HashSet<String>> backloopedge) {
		this.backloopedge = backloopedge;
	}
	
	
}
