package com.uantwerp.rinternship.main;

public class EdgePair {

	private String SourceId;
	private String SourceLabel;
	private String TargetId;
	private String TargetLabel;
	
	
	public EdgePair(String sourceId, String sourceLabel, String targetId, String targetLabel) {
		super();
		SourceId = sourceId;
		SourceLabel = sourceLabel;
		TargetId = targetId;
		TargetLabel = targetLabel;
	}
	
	public String getSourceId() {
		return SourceId;
	}
	
	public void setSourceId(String sourceId) {
		SourceId = sourceId;
	}
	
	public String getSourceLabel() {
		return SourceLabel;
	}
	
	public void setSourceLabel(String sourceLabel) {
		SourceLabel = sourceLabel;
	}
	
	public String getTargetId() {
		return TargetId;
	}
	
	public void setTargetId(String targetId) {
		TargetId = targetId;
	}
	
	public String getTargetLabel() {
		return TargetLabel;
	}
	
	public void setTargetLabel(String targetLabel) {
		TargetLabel = targetLabel;
	}
	

	
	
}
