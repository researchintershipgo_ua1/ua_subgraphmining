package com.uantwerp.rinternship.main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.uantwerp.rinternship.main.procedures.naive.BuildMotif;
import com.uantwerp.rinternship.utilities.AlgorithmUtility;
import com.uantwerp.rinternship.utilities.FileUtility;
import com.uantwerp.rinternship.utilities.PrintUtility;

public class AlgorithmFunctionality {

	
	public AlgorithmFunctionality() {
	}

	public void mainProcedure(){
		HashGeneration.graphGeneration();
		if (GraphPathParameters.debug == 1) PrintUtility.printSummary();
		GraphPathParameters.supportcutoff = AlgorithmUtility.supportTreshold();
		iterateOverGroups();
	}
			
	private static void iterateOverGroups(){
		for (int i = 0; i<Graph.possibleLabels.size(); i++){
			if (GraphPathParameters.undirected == 0){
				List<EdgePair> motif = new ArrayList<>();
				motif.add(new EdgePair("1",Graph.possibleLabels.get(i),"1",Graph.possibleLabels.get(i)));
				BuildMotif.build_motif(motif,Graph.bgnodes);
			}
			for (int j = 0; j < Graph.possibleLabels.size(); j++){
				if (GraphPathParameters.undirected == 1){
					List<EdgePair> forwardmotif = new ArrayList<>();
					forwardmotif.add(new EdgePair("1",Graph.possibleLabels.get(i),"2",Graph.possibleLabels.get(j)));
					BuildMotif.build_motif_und(forwardmotif,Graph.bgnodes);
				}else{
					//Single edge to start building from
					List<EdgePair> forwardmotif = new ArrayList<>();
					forwardmotif.add(new EdgePair("1",Graph.possibleLabels.get(i),"2",Graph.possibleLabels.get(j)));
					BuildMotif.build_motif(forwardmotif,Graph.bgnodes);
					//Single edge to start building from
					List<EdgePair> backwardmotif = new ArrayList<>();
					backwardmotif.add(new EdgePair("2",Graph.possibleLabels.get(i),"1",Graph.possibleLabels.get(j)));
					BuildMotif.build_motif(backwardmotif,Graph.bgnodes);
				}
			}
		}
		recalculateAndPrintResults();
	}
	
	
	
	public static void printGraphVariables(){
		System.out.println("print graph");
		PrintUtility.printHasMapHashSet(Graph.graphHash);
		System.out.println("print reverse graph");
		PrintUtility.printHasMapHashSet(Graph.reverseGraphHash);
		System.out.println("print labels");
		PrintUtility.printHasMapHashSet(Graph.label);
		System.out.println("print reverse labels");
		PrintUtility.printHasMapHashSet(Graph.reverseLabel);
		System.out.println("print possible labels");
		PrintUtility.printListString(Graph.possibleLabels);
		System.out.println("print group, size: " + Graph.group.size());
		PrintUtility.printHSetString(Graph.group);
		System.out.println("print bgnodes");
		PrintUtility.printHashSet(Graph.bgnodes);
		System.out.println("Label hash");
		PrintUtility.printHasMap2(Graph.labelHash);
	}
	
	private static void recalculateAndPrintResults(){
		if (!Graph.group.isEmpty()){
			Double bonferonni = GraphPathParameters.pvalue/MiningState.sigmotifs.size();
			if (GraphPathParameters.debug == 1) System.out.println("Checked: "+MiningState.sigmotifs.size()+" subgraphs");
			if (GraphPathParameters.debug == 1) System.out.println("Bonferonni-corrected P-value cutoff =  "+bonferonni);
			Iterator<String> it = MiningState.sigmotifs.keySet().iterator();
			String message ="Motif \t FreqS \t FreqT \t Pvalue \t";
			while (it.hasNext()){
				String key = it.next();
				if (MiningState.sigmotifs.get(key) <= bonferonni)
					message = message+"\n"+key+"\t"+MiningState.checkedmotifs.get(key)+"\t"+MiningState.freqmotifs.get(key)+"\t"+MiningState.sigmotifs.get(key);
			}
			if (!GraphPathParameters.output.equals("none")){
				FileUtility.writeFile(GraphPathParameters.output, message.replace("~", ""));
			}else{
				System.out.println(message.replace("~", ""));
			}
		}else{
			Iterator<String> it = MiningState.freqmotifs.keySet().iterator();
			while (it.hasNext()){
				String key = it.next();
				System.out.println(key+"\t"+MiningState.freqmotifs.get(key));
			}
		}
	}
	

	
}
