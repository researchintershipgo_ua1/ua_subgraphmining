package com.uantwerp.rinternship.main;

import java.io.BufferedReader;
//import java.io.IOException;
import java.io.StringReader;
import java.util.Iterator;

import com.uantwerp.rinternship.exceptions.SubGraphMiningException;
import com.uantwerp.rinternship.utilities.AlgorithmUtility;

public class HashGeneration {
	
	public static void graphGeneration(){		
		readGraph(GraphPathParameters.pathGraph, 1);
		readGraph(GraphPathParameters.pathLabels, 2);
		readGraph(GraphPathParameters.pathGroupFile, 3);
		readGraph(GraphPathParameters.pathBgNodes, 4);
		Graph.possibleLabels = AlgorithmUtility.getPossibleLables(GraphPathParameters.singleLabel,Graph.reverseLabel);
	}

	public static void readGraph(String file, int type){
		if (file == null){
			file="";
		}
		BufferedReader reader = new BufferedReader(new StringReader(file));
		if (type==1)
			AlgorithmUtility.checkGraphFile(reader.toString());
		String[] lines = file.split("\r\n|\r|\n");
		for (String line : lines) {
			generateHashTable(type, line);
		}
	}
	
	public static void generateHashTable(int type, String node){
		if (node != null){
			String[] nodeArray = new String[2];
			if (node != null){
				nodeArray = node.split("\t");
			}
			if (type==1){
				Graph.graphHash = AlgorithmUtility.addCheckElementHash(Graph.graphHash,nodeArray[0],nodeArray[1]);
				Graph.reverseGraphHash = AlgorithmUtility.addCheckElementHash(Graph.reverseGraphHash,nodeArray[1],nodeArray[0]);
				if (GraphPathParameters.singleLabel == 0){
					Graph.label = AlgorithmUtility.addCheckElementHash(Graph.label,nodeArray[0],"~");
					Graph.label = AlgorithmUtility.addCheckElementHash(Graph.label,nodeArray[1],"~");
					if (!Graph.labelHash.containsKey("~"))
						Graph.labelHash.put("~", 1);
					else
						Graph.labelHash.put("~", Graph.labelHash.get("~")+1);
				}
			}else if (type==2){
				if (AlgorithmUtility.checkEmptyGraph(GraphPathParameters.pathLabels)){
					Graph.reverseLabel = AlgorithmUtility.addCheckElementHash(Graph.reverseLabel,nodeArray[1],nodeArray[0]);
					Graph.label = AlgorithmUtility.addCheckElementHash(Graph.label,nodeArray[0],nodeArray[1]);
					if (!Graph.labelHash.containsKey(nodeArray[1]))
						Graph.labelHash.put(nodeArray[1], 1);
					else
						Graph.labelHash.put(nodeArray[1], Graph.labelHash.get(nodeArray[1])+1);
				}else{
					if (GraphPathParameters.singleLabel == 1)
						SubGraphMiningException.exceptionNoFile("Labels file is needed when the single label parameter is activated");					
				}
			}else if (type==3){
				if (AlgorithmUtility.checkEmptyGraph(GraphPathParameters.pathGroupFile)){
					if (Graph.label.containsKey(nodeArray[0])){
						Graph.group.add(nodeArray[0]);
					}else
						SubGraphMiningException.exceptionNoVertexInLabels(nodeArray[0]);
				}else{
					Graph.group = AlgorithmUtility.returnKeySetHash(Graph.label);
					Iterator<String> it = Graph.label.keySet().iterator();
					while (it.hasNext())
						Graph.bgnodes.add(it.next());
				}
			}else if (type==4){
				if (AlgorithmUtility.checkEmptyGraph(GraphPathParameters.pathGroupFile)){
					if (GraphPathParameters.pathBgNodes != null){
						if (AlgorithmUtility.checkEmptyGraph(GraphPathParameters.pathBgNodes))
							Graph.bgnodes.add(nodeArray[0]);
					}else{
						Iterator<String> it = Graph.label.keySet().iterator();
						while (it.hasNext())
							Graph.bgnodes.add(it.next());
					}
				}
			}
		}
	}
	
}
