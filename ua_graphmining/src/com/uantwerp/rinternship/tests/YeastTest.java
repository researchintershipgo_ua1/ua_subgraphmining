package com.uantwerp.rinternship.tests;

import org.junit.Test;

import com.uantwerp.rinternship.main.SubgraphMining;
import com.uantwerp.rinternship.utilities.FileUtility;

public class YeastTest {
	
	public static String pathGraph = "./yeastract_edges.txt";
	public static String pathLabels = "./yeast_gocat_mutiple.txt";
	public static String pathVertexSet = "./node_duplicate.txt";
	public static String pathOutput = "./yeast_gocat_maxsize2_multi_java.txt";
	
	@Test
	public void testSGMiningProcedure() {
		String graph = FileUtility.getFile(pathGraph);
		String labels = FileUtility.getFile(pathLabels);
		String vertex = FileUtility.getFile(pathVertexSet);
		new SubgraphMining(graph, labels,vertex,null,
							0, //suport
							0, //singlelabel
							0, //undirected
							2, //maxsize
							1, //debug
							0.05, //pval
							0, //nestedpval
							pathOutput);

	}
}
