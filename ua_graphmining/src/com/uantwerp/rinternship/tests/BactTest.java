package com.uantwerp.rinternship.tests;

import org.junit.Test;

import com.uantwerp.rinternship.main.SubgraphMining;
import com.uantwerp.rinternship.utilities.FileUtility;

public class BactTest {
	
	public static String pathGraph = "./full_net.txt";
	public static String pathLabels = "";
	public static String pathinterestingNodes = "./tfs.txt";
	public static String pathVertexSet = "./phor.txt";
	public static String pathOutput = "./phor_bg_max_4no_bf_java.txt";
	
	@Test
	public void testSGMiningProcedure() {
		String graph = FileUtility.getFile(pathGraph);
		String labels = FileUtility.getFile(pathLabels);
		String vertex = FileUtility.getFile(pathVertexSet);
		String bgFile = FileUtility.getFile(pathinterestingNodes);
		new SubgraphMining(graph, labels, vertex, bgFile, 
							10, //suport
							0, //singlelabel
							0, //undirected
							4, //maxsize
							1, //debug
							0.05, //pval
							0, //nestedpval
							pathOutput);
		
	}
}
