package com.uantwerp.rinternship.tests;

import org.junit.Test;

import com.uantwerp.rinternship.main.SubgraphMining;
import com.uantwerp.rinternship.utilities.FileUtility;

public class BasicTest {
	
	public static String pathGraph = "/Users/gerardo/Google Drive/antwerp_university/thesis/code/datasets/example/example_graph.txt";
	public static String pathLabels = "/Users/gerardo/Google Drive/antwerp_university/thesis/code/datasets/example/example_labels.txt";
	public static String pathVertexSet = "/Users/gerardo/Google Drive/antwerp_university/thesis/code/datasets/example/example_vertexset.txt";
	
//	public SubgraphMining(String graphPath, String labelsPath, String groupfilePath, String bgFilePath, 
//			int support, int singleLabel, int undirected, int maxsize, int debug,
//			double pvalue, int nestedpval, String output) {
	@Test
	public void testSGMiningProcedure() {
		String graph = FileUtility.getFile(pathGraph);
		String labels = FileUtility.getFile(pathLabels);
		String vertex = FileUtility.getFile(pathVertexSet);
		new SubgraphMining(graph, labels,vertex,null,0,0,0,3,1,0.05,0,"none");
	}
}
