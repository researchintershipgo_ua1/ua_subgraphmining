package com.uantwerp.rinternship.tests;

import org.junit.Test;

import com.uantwerp.rinternship.main.SubgraphMining;
import com.uantwerp.rinternship.utilities.FileUtility;

public class SSMTest {
	
	public static String pathGraph = "/Users/gerardo/Google Drive/antwerp_university/thesis/code/base_code/datasets/pdb/SSM_GR.txt";
	public static String pathLabels = "/Users/gerardo/Google Drive/antwerp_university/thesis/code/base_code/datasets/pdb/SSM_LA.txt";
	public static String pathVertexSet = "/Users/gerardo/Google Drive/antwerp_university/thesis/code/base_code/datasets/pdb/SSM_MN3.txt";
	
	@Test
	public void testSGMiningProcedure() {
		String graph = FileUtility.getFile(pathGraph);
		String labels = FileUtility.getFile(pathLabels);
		String vertex = FileUtility.getFile(pathVertexSet);
		new SubgraphMining(graph, labels,vertex,null,
							0, //suport
							0, //singlelabel
							1, //undirected
							1, //maxsize
							1, //debug
							0.05, //pval
							0, //nestedpval
							"none");
	}
}
